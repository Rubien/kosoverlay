KOSOverlay
====================

EVE, NRDS & Sev3rance - Prologue
---------------------

This tool is for the space combat game EVE Online by CCP.
This is an MMORPG, where much of the content is user-driven
(aka. a sandbox).

-7- (Sev3rance) is an alliance that resides in the
Providence region of zero security space. Unknown
pilots can use this space, as long as they are not
known to be, or belong to, a hostile entity.
This policy is rare in EVE and known as NRDS (Not Red
Don't Shoot). Most areas of zero security space are
very hostile, and operate under NBSI (Not Blue Shoot It).
It goes without saying, when everyone but people you know
are deemed hostile, it is much easier to assess solar
systems entered.

It is hard to implement NRDS, as it involves checking
unknown pilots against a database, applying a series of
rules, before knowing whether this pilot is to be treated
with hostility. This can be costly and confusing, and
the delay can put you in danger.

This tool aims to reduce this pain via a prompt, keyboard
listening, overlay for the client.

Summary - EVE Players
---------------------

This tool is designed for use with EVE online in Windowed
mode. In this fashion, it can sit on top of the client,
unlike when EVE is running in full screen.

The idea is to enable quick lookups of 'Kill On Sight'
(KOS) status of multiple pilots in local.
Since it's an overlay, we normally want immediate
but transient feedback. It can't dominate the screen,
and the space we do use shouldn't be held longer
than needed.

This is built around the CVA KOS Checker API, and should
be usable by anyone in the CVA Coalition.