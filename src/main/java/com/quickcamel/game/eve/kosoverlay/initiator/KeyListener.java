/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.initiator;

import com.quickcamel.game.eve.kosoverlay.service.IKOSBatchService;
import javafx.application.Platform;
import javafx.scene.input.Clipboard;
import org.jnativehook.NativeInputEvent;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Arrays;

/**
 * The key listener that starts the KOS checks
 *
 * @author Louis Burton
 */
public class KeyListener implements NativeKeyListener {

    private static final Logger m_logger = LoggerFactory.getLogger(KeyListener.class);

    @Inject
    private IKOSBatchService m_batchService;

    private Clipboard m_clipboard;

    private long m_lastCopy = 0;

    @PostConstruct
    public void initialise() {
        Platform.runLater(new Runnable() {
            public void run() {
                m_clipboard = Clipboard.getSystemClipboard();
            }
        });
    }

    @Override
    public void nativeKeyPressed(NativeKeyEvent nativeKeyEvent) {
        if (nativeKeyEvent.getKeyCode() == NativeKeyEvent.VK_C && nativeKeyEvent.getModifiers() == NativeInputEvent.CTRL_MASK) {
            long now = System.currentTimeMillis();
            if (now - m_lastCopy > 1000) {
                m_lastCopy = now;
            }
            else {
                m_logger.debug(nativeKeyEvent.toString());
                m_lastCopy = 0;
                runFromClipboard();
            }
        }
    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent nativeKeyEvent) {
        if (nativeKeyEvent.getKeyCode() == NativeKeyEvent.VK_CONTROL && m_lastCopy > 0) {
            m_lastCopy = 0;
        }
    }

    @Override
    public void nativeKeyTyped(NativeKeyEvent nativeKeyEvent) {
        // do nothing
    }

    public void runFromClipboard() {
        Platform.runLater(new Runnable() {
            public void run() {
                String kosString = m_clipboard.getString();
                m_logger.debug(kosString);
                if (kosString != null) {
                    final String[] pilotNames = kosString.split("\n|  ");
                    m_batchService.startBatch(Arrays.asList(pilotNames));
                }
            }
        });
    }
}
