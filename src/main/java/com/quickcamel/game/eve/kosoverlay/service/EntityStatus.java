/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service;

/**
 * The possible result status of a given Pilot, Corporation or Alliance
 *
 * @author Louis Burton
 */
public enum EntityStatus {
    UNKNOWN(0),
    NEUTRAL(1),
    NEUTRAL_STANDINGS(2),
    BLUE_5(2),
    BLUE_10(2),
    RED_5(2),
    RED_10(2),
    BLUE_SAME_ALLIANCE(4),
    BLUE_SAME_PILOT_OR_CORP(5),
    KOS_CVA_CONFLICT(7),
    KOS_CVA(8),
    ERROR(99);

    private int m_weight;

    EntityStatus(int weight) {
        m_weight = weight;
    }

    public static boolean isKOS(EntityStatus status) {
        return status == KOS_CVA || status == KOS_CVA_CONFLICT || status == RED_5 || status == RED_10;
    }

    public static boolean isBlue(EntityStatus status) {
        return status == BLUE_10 || status == BLUE_5;
    }

    public boolean overrules(EntityStatus status) {
        return m_weight > status.m_weight;
    }
}
