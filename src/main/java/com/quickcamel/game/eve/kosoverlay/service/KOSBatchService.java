/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service;

import com.quickcamel.game.eve.kosoverlay.configuration.*;
import com.quickcamel.game.eve.kosoverlay.service.filter.IPilotFilter;
import com.quickcamel.game.eve.kosoverlay.service.tasks.KOSCheckTask;
import com.quickcamel.game.eve.kosoverlay.service.user.ICurrentUserContext;
import com.quickcamel.game.eve.kosoverlay.view.fx.UIConstants;
import com.quickcamel.game.eve.kosoverlay.view.fx.CounterProperty;
import com.quickcamel.game.eve.kosoverlay.view.fx.KOSTaskDisplay;
import com.quickcamel.game.eve.kosoverlay.view.fx.controllers.OverlayController;
import javafx.animation.*;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.concurrent.Worker;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.media.AudioClip;
import javafx.scene.text.Text;
import javafx.util.Duration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.inject.Provider;
import java.io.File;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Services a batch of pilot check requests.
 * Does depend on an OverlayController for displaying its results, this is not decoupled from all view aspects currently
 *
 * @author Louis Burton
 */
public class KOSBatchService implements IKOSBatchService, IConfigChangeListener {

    private static final Logger m_logger = LoggerFactory.getLogger(KOSBatchService.class);
    private static final AudioClip DEFAULT_KOS_SOUND = new AudioClip(KOSTaskDisplay.class.getResource("alert.wav").toString());
    private static AudioClip CUSTOM_KOS_SOUND = null;

    private int m_maximumDisplay;
    private int m_largeBatchThreshold;

    @Resource(name = "resultsList")
    private ObservableList<KOSTaskDisplay> m_resultsList;

    @Inject
    private Provider<KOSCheckTask> m_taskProvider;

    @Resource(name = "validPilotFilter")
    private IPilotFilter m_validPilotNameFilter;

    @Resource(name = "pilotStandingsFilter")
    private IPilotFilter m_pilotStandingsFilter;

    @Resource(name = "kosCheckExecutorService")
    private ExecutorService m_executorService;

    @Inject
    private OverlayController m_overlayController;

    @Inject
    private Provider<KOSTaskDisplay> m_taskDisplayProvider;

    @Inject
    private IConfigManager m_configManager;

    @Inject
    private ICurrentUserContext m_userContext;

    private Set<KOSTaskDisplay> m_lastResults = new HashSet<>();
    private boolean m_holding = false;

    // Filtering UI components
    private final HBox m_filterHeader = new HBox();
    private final ImageView m_filterLoadingImage = new ImageView(UIConstants.LOADING_IMAGE);
    private final Text m_filterText = new Text();

    // Large batch variables
    // UI components
    private final HBox m_header = new HBox();
    private final ImageView m_headerLoadingImage = new ImageView(UIConstants.LOADING_IMAGE);
    private final Text m_headerText = new Text();
    private final Button m_headerCancelButton = new Button();
    private Transition m_headerTransition;

    // UI counters and controls
    private final CounterProperty m_progressCount = new CounterProperty();
    private final CounterProperty m_total = new CounterProperty();
    private final CounterProperty m_errors = new CounterProperty();
    private final BooleanProperty m_foundError = new SimpleBooleanProperty(false);

    // Concurrency devices
    // This lock is quite expensive and is due to the transitions into and out of a large batch, so we know when one is running or not without racing against an initialisation or finalisation
    // - it is pessimistic and a worst case scenario precaution, it could be configured for switching off in future
    private final Semaphore m_largeBatchLock = new Semaphore(1, true);
    private final AtomicBoolean m_processingLargeBatch = new AtomicBoolean(false);
    private final AtomicBoolean m_kosSoundPlayedForBatch = new AtomicBoolean(false);

    private Set<KOSCheckTask> m_largeBatch = new HashSet<>();


    public KOSBatchService() {
        m_header.setAlignment(Pos.CENTER_LEFT);
        m_header.setPadding(new Insets(0, 0, 0, 10));
        m_header.setMaxHeight(28);
        m_header.setMinHeight(28);
        m_header.setMaxWidth(350);
        m_header.setMinWidth(350);
        m_header.setSpacing(6);
        m_header.setStyle(" -fx-opacity: 0.92");
        m_headerText.getStyleClass().add(UIConstants.BASE_TEXT_STYLE);
        m_headerText.textProperty().bind(Bindings
                .when(m_foundError)
                .then(Bindings.format("%d/%d    %d error(s)", m_progressCount, m_total, m_errors))
                .otherwise(Bindings.format("%d/%d", m_progressCount, m_total)));
        m_headerCancelButton.setText("X");
        m_headerCancelButton.setId("cancel-button");
        m_headerCancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                m_logger.debug("CANCEL");
                stopLargeBatchIfNeeded(true);
            }
        });
        m_header.getChildren().addAll(m_headerLoadingImage, m_headerCancelButton, m_headerText);
        initFilterHeader();
    }

    private void initFilterHeader() {
        m_filterHeader.setAlignment(Pos.CENTER_LEFT);
        m_filterHeader.setPadding(new Insets(0, 0, 0, 10));
        m_filterHeader.setMaxHeight(28);
        m_filterHeader.setMinHeight(28);
        m_filterHeader.setMaxWidth(350);
        m_filterHeader.setMinWidth(350);
        m_filterHeader.setSpacing(6);
        m_filterHeader.setStyle(" -fx-opacity: 0.95");
        m_filterText.getStyleClass().add(UIConstants.BASE_TEXT_STYLE);
        m_filterText.getStyleClass().add(UIConstants.LOADING_STYLE);
        m_filterText.setText("Filtering...");
        m_filterHeader.getChildren().addAll(m_filterLoadingImage, m_filterText);
    }

    @PostConstruct
    private void initialise() {
        m_maximumDisplay = Integer.parseInt(m_configManager.getValue(ConfigKeyConstants.RESULTS_MAXIMUM, ConfigKeyConstants.RESULTS_MAXIMUM_DEFAULT));
        m_largeBatchThreshold = Integer.parseInt(m_configManager.getValue(ConfigKeyConstants.BATCHSERVICE_LARGEBATCH_THRESHOLD, ConfigKeyConstants.BATCHSERVICE_LARGEBATCH_THRESHOLD_DEFAULT));
        String customKosSound = m_configManager.getValue(ConfigKeyConstants.CUSTOM_KOS_SOUND);
        if (StringUtils.isEmpty(customKosSound)) {
            CUSTOM_KOS_SOUND = null;
        }
        else {
            CUSTOM_KOS_SOUND = new AudioClip(new File(customKosSound).toURI().toASCIIString());
        }
        long fadePauseDuration = Long.valueOf(m_configManager.getValue(ConfigKeyConstants.FADE_PAUSE_DURATION, ConfigKeyConstants.FADE_PAUSE_DURATION_DEFAULT));

        final PauseTransition pauseTransition = new PauseTransition(Duration.millis(fadePauseDuration));
        final FadeTransition fadeTransition = new FadeTransition(Duration.millis(UIConstants.FADE_TRANSITION_DURATION), m_header);
        fadeTransition.setFromValue(0.75);
        fadeTransition.setToValue(0.0);
        m_headerTransition = new SequentialTransition(pauseTransition, fadeTransition);
        m_headerTransition.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                m_logger.debug("Fading finished of large batch header : totalDuration=" + m_headerTransition.getTotalDuration());
                m_overlayController.getResultPanel().getChildren().remove(m_header);
            }
        });
    }


    @Override
    public void startBatch(final Collection<String> pilotNames) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Set<String> validPilots = m_validPilotNameFilter.getFilteredPilots(pilotNames);
                if (Boolean.valueOf(m_configManager.getValue(ConfigKeyConstants.BATCHSERVICE_ONLYCHECKNOSTANDINGS, ConfigKeyConstants.BATCHSERVICE_ONLYCHECKNOSTANDINGS_DEFAULT))) {
                    validPilots = filterOutKnownPilots(validPilots);
                }

                if (m_logger.isDebugEnabled()) {
                    m_logger.debug(validPilots.toString());
                }
                if (validPilots.size() > 0) {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            m_overlayController.removeSidePanels();
                        }
                    });
                    try {
                        // If the the new batch would push the display over the large batch threshold - process differently as a large batch
                        if ((validPilots.size() + m_resultsList.size()) <= m_largeBatchThreshold) {
                            startSmallBatch(validPilots);
                        }
                        else {
                            m_largeBatchLock.acquire();
                            // Start the large batch, or add to an already running large batch
                            if (!m_processingLargeBatch.getAndSet(true)) {
                                // Start large batch
                                if (m_logger.isDebugEnabled()) {
                                    m_logger.debug("STARTING large batch : size=" + validPilots.size() + ", hash=" + validPilots.hashCode());
                                }
                                initialiseLargeBatch();
                                m_largeBatchLock.release();
                                startLargeBatch(validPilots);
                            }
                            else {
                                // Add to large batch
                                if (m_logger.isDebugEnabled()) {
                                    m_logger.debug("ADDING to large batch : size=" + validPilots.size() + ", hash=" + validPilots.hashCode());
                                }
                                Set<KOSCheckTask> largeBatchAdditions = prepareLargeBatchAdditions(validPilots);
                                m_largeBatchLock.release();
                                addToLargeBatch(largeBatchAdditions);
                            }
                        }
                    }
                    catch (InterruptedException e) {
                        m_logger.error("Batch interrupted - " + validPilots.hashCode(), e);
                        m_largeBatchLock.release();
                    }
                }
            }
        }).start();
    }

    private Set<String> filterOutKnownPilots(Set<String> validPilots) {
        m_logger.debug("Filtering out pilots with standings.");
        Platform.runLater(new Runnable() {
            public void run() {
                m_overlayController.removeSidePanels();
                if (!m_overlayController.getResultPanel().getChildren().contains(m_filterHeader)) {
                    m_overlayController.getResultPanel().getChildren().add(0, m_filterHeader);
                }
            }
        });
        validPilots = m_pilotStandingsFilter.getFilteredPilots(validPilots);
        Platform.runLater(new Runnable() {
            public void run() {
                m_overlayController.getResultPanel().getChildren().remove(m_filterHeader);
            }
        });
        return validPilots;
    }

    private void startLargeBatch(final Set<String> validPilots) {
        final Set<KOSCheckTask> largeBatch = new HashSet<>();
        final Set<KOSCheckTask> assimilatedTasks = new HashSet<>();
        final Set<KOSTaskDisplay> taskDisplaysToRemove = new HashSet<>();
        // Assimilate existing results - unless fading or stale held tasks
        for (KOSTaskDisplay existingDisplay : m_resultsList) {
            if ((m_holding && existingDisplay.getTask().isDone())
                    || !existingDisplay.stopFadingIfNotStarted()) {
                taskDisplaysToRemove.add(existingDisplay);
            }
            else {
                m_total.increment();
                largeBatch.add(existingDisplay.getTask());
                assimilatedTasks.add(existingDisplay.getTask());
            }
        }

        // Remove all non-assimilated tasks, and displayed non-KOS tasks that have been assimilated
        Platform.runLater(new Runnable() {
            public void run() {
                m_resultsList.removeAll(taskDisplaysToRemove);
                Iterator<KOSTaskDisplay> iterator = m_resultsList.iterator();
                while (iterator.hasNext()) {
                    KOSTaskDisplay existingDisplay = iterator.next();
                    if (!EntityStatus.isKOS(existingDisplay.getTask().getKOSStatusSummary().getResolvedStatus())) {
                        iterator.remove();
                    }
                }
            }
        });

        // Create Tasks for doing the newly requested checks
        for (String pilotName : validPilots) {
            final KOSCheckTask task = m_taskProvider.get();
            task.setPilotName(pilotName);
            m_total.increment();
            largeBatch.add(task);
        }
        m_largeBatch = largeBatch;
        // Initialise header
        m_headerText.getStyleClass().removeAll(UIConstants.OVERLAY_STYLES);
        m_headerText.getStyleClass().add(UIConstants.LOADING_STYLE);
        m_headerLoadingImage.setVisible(true);
        m_headerCancelButton.setVisible(true);

        Platform.runLater(new Runnable() {
            public void run() {
                // Remove just in case and re-add for proper display
                removeHeader();
                m_overlayController.getResultPanel().getChildren().add(0, m_header);
                for (final KOSCheckTask task : largeBatch) {
                    // Set handlers before assessing if done - don't want to synchronize all threads further, and double counting is safer than not counting
                    task.setOnSucceeded(getLargeBatchSuccessHandler(task, assimilatedTasks));
                    task.setOnFailed(getLargeBatchErrorHandler(task));
                    if (task.isDone()) {
                        m_progressCount.increment();
                    }
                    if (!task.isRunning()) {
                        m_executorService.execute(task);
                    }
                }
            }
        });
    }

    private Set<KOSCheckTask> prepareLargeBatchAdditions(final Set<String> validPilots) {
        final Set<KOSCheckTask> additionalLargeBatchTasks = new HashSet<>();
        for (String pilotName : validPilots) {
            // Create Task for doing the newly requested checks
            final KOSCheckTask task = m_taskProvider.get();
            task.setPilotName(pilotName);
            m_total.increment();
            m_largeBatch.add(task);
            additionalLargeBatchTasks.add(task);
        }
        return additionalLargeBatchTasks;
    }

    private void addToLargeBatch(final Set<KOSCheckTask> additionalLargeBatchTasks) {
        Platform.runLater(new Runnable() {
            public void run() {
                for (final KOSCheckTask task : additionalLargeBatchTasks) {
                    // Set handlers before assessing if done - don't want to synchronize all threads, and double counting is safer than not counting
                    task.setOnSucceeded(getLargeBatchSuccessHandler(task, null));
                    task.setOnFailed(getLargeBatchErrorHandler(task));
                    m_executorService.execute(task);
                }
            }
        });
    }

    private EventHandler<WorkerStateEvent> getLargeBatchSuccessHandler(final KOSCheckTask task,
                                                                       final Set<KOSCheckTask> existingTasks) {
        return new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent workerStateEvent) {
                m_progressCount.increment();
                if (m_logger.isDebugEnabled()) {
                    m_logger.debug("Large Batch SUCCESS : " + task + ", count=" + m_progressCount.get() + ", total=" + m_total.get() + ", hash=" + m_largeBatch.hashCode());
                }
                boolean onlyNeuts = Boolean.valueOf(m_configManager.getValue(ConfigKeyConstants.BATCHSERVICE_LARGEBATCH_NONEGATIVESTANDINGS, ConfigKeyConstants.BATCHSERVICE_LARGEBATCH_NONEGATIVESTANDINGS_DEFAULT));
                EntityStatus resolvedStatus = task.getKOSStatusSummary().getResolvedStatus();
                // If KOS (or a neut with a warning) - and we are not filtering out pilots with negative standings; add to display
                if ((EntityStatus.isKOS(resolvedStatus)
                        || (task.getKOSStatusSummary().isWarning() && resolvedStatus.equals(EntityStatus.NEUTRAL)))
                        && (!onlyNeuts || !m_userContext.hasNegativeStandings(task.getPilotKOSDetails()))) {
                    if (m_resultsList.size() < m_maximumDisplay && (existingTasks == null || !existingTasks.contains(task))) {
                        final KOSTaskDisplay taskDisplay = m_taskDisplayProvider.get();
                        taskDisplay.setTask(task);
                        taskDisplay.updateGraphicFromState();
                        playKOSSoundIfRequired(true);
                        m_resultsList.add(taskDisplay);
                    }
                }
                else if (resolvedStatus == EntityStatus.ERROR) {
                    m_errors.increment();
                    m_foundError.set(true);
                }
                stopLargeBatchIfNeeded(false);
            }
        };
    }

    private EventHandler<WorkerStateEvent> getLargeBatchErrorHandler(final KOSCheckTask task) {
        return new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent workerStateEvent) {
                m_progressCount.increment();
                if (m_logger.isDebugEnabled()) {
                    m_logger.debug("Large Batch FAILED : " + task + ", count=" + m_progressCount.get());
                }
                m_errors.increment();
                m_foundError.set(true);
                stopLargeBatchIfNeeded(false);
            }
        };
    }

    private boolean removeHeader() {
        boolean headerWasShowing;
        m_headerTransition.stop();
        if (headerWasShowing = m_overlayController.getResultPanel().getChildren().contains(m_header)) {
            m_overlayController.getResultPanel().getChildren().remove(m_header);
        }
        m_overlayController.getResultPanel().getChildren().remove(m_filterHeader);
        m_header.setStyle(" -fx-opacity: 0.92");
        return headerWasShowing;
    }

    private void initialiseLargeBatch() {
        m_progressCount.set(0);
        m_total.set(0);
        m_errors.set(0);
        m_foundError.setValue(false);
    }

    private void stopLargeBatchIfNeeded(boolean force) {
        try {
            m_largeBatchLock.acquire();
            // If it's not processing a large batch anymore,
            // or the results are within limit, the total hasn't been hit, and this isn't a forced stop - do nothing
            if (!m_processingLargeBatch.get() ||
                    (m_resultsList.size() < m_maximumDisplay
                            && m_progressCount.get() != m_total.get()
                            && !force)) {
                if (m_logger.isDebugEnabled()) {
                    m_logger.debug("Large Batch does not need stopping : hash=" + m_largeBatch.hashCode());
                }
            }
            else {
                if (m_logger.isDebugEnabled()) {
                    int kosCount = 0;
                    for (KOSCheckTask task : m_largeBatch) {
                        if (task.getKOSStatusSummary() != null && EntityStatus.isKOS(task.getKOSStatusSummary().getResolvedStatus())) {
                            kosCount++;
                        }
                    }
                    m_logger.debug("STOPPING large batch : size=" + m_largeBatch.size() + ", hash=" + m_largeBatch.hashCode() + ", kosCount=" + kosCount);
                }
                m_headerText.getStyleClass().removeAll(UIConstants.OVERLAY_STYLES);
                if (force) {
                    m_headerText.getStyleClass().add(UIConstants.FAILED_STYLE);
                }
                else if (m_resultsList.size() > 0) {
                    m_headerText.getStyleClass().add(UIConstants.KOS_STYLE);
                    m_headerText.getStyleClass().add(UIConstants.RED_STYLE);
                }
                else {
                    m_headerText.getStyleClass().add(UIConstants.NEUTRAL_STYLE);
                }
                m_headerLoadingImage.setVisible(false);
                m_headerCancelButton.setVisible(false);
                m_lastResults = new HashSet<>();
                m_lastResults.addAll(m_resultsList);
                if (!m_holding) {
                    for (KOSTaskDisplay kosTaskDisplay : m_resultsList) {
                        kosTaskDisplay.fadeOut(false);
                    }
                }
                if (m_progressCount.get() != m_total.get()) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Set<KOSCheckTask> cancelledBatch = m_largeBatch;
                            if (m_logger.isDebugEnabled()) {
                                m_logger.debug("Cancelling : " + cancelledBatch);
                            }
                            m_largeBatch = new HashSet<>();
                            for (KOSCheckTask task : cancelledBatch) {
                                if (!task.isDone()) {
                                    task.cancel(true);
                                }
                            }
                        }
                    }).run();
                }
                m_headerTransition.playFromStart();
                m_kosSoundPlayedForBatch.set(false);
                m_processingLargeBatch.set(false);
            }
        }
        catch (InterruptedException e) {
            m_logger.error("Stop Batch check interrupted - " + m_largeBatch.hashCode(), e);
        }
        finally {
            m_largeBatchLock.release();
        }
    }

    private void startSmallBatch(final Set<String> validPilots) {
        if (m_logger.isDebugEnabled()) {
            m_logger.debug("Small Batch : " + validPilots.toString());
        }
        // Setup tasks
        final ProgressBar progressIndicator = new ProgressBar();
        final Set<KOSTaskDisplay> taskDisplays = new HashSet<>();
        for (String pilotName : validPilots) {
            // Create Task for doing the check
            final KOSCheckTask task = m_taskProvider.get();
            task.setPilotName(pilotName);
            // Create Display for showing the check, and associate with Task
            final KOSTaskDisplay taskDisplay = m_taskDisplayProvider.get();
            taskDisplay.setTask(task);
            taskDisplays.add(taskDisplay);
        }
        m_lastResults = taskDisplays;


        final ChangeListener<Number> progressChangeListener = getSmallBatchProgressChangeListener(taskDisplays, progressIndicator);
        if (m_logger.isDebugEnabled()) {
            m_logger.debug("Batch Tasks : " + getBatchStringFromDisplays(taskDisplays));
        }

        Platform.runLater(new Runnable() {
            public void run() {
                if (removeHeader()) {
                    m_resultsList.clear();
                }
                else if (m_holding) {
                    // Remove stale held tasks
                    Iterator<KOSTaskDisplay> iterator = m_resultsList.iterator();
                    while (iterator.hasNext()) {
                        KOSTaskDisplay display = iterator.next();
                        if (display.getTask().isDone()) {
                            iterator.remove();
                        }
                    }
                }
                // Monitor overall progress
                DoubleBinding progress = null;
                for (KOSTaskDisplay taskDisplay : taskDisplays) {
                    // here goes binding creation
                    DoubleBinding scaledProgress = taskDisplay.getTask().progressProperty().divide(validPilots.size());
                    if (progress == null) {
                        progress = scaledProgress;
                    }
                    else {
                        progress = progress.add(scaledProgress);
                    }
                }
                progressIndicator.progressProperty().bind(progress);
                if (progress != null) {
                    progress.addListener(progressChangeListener);
                }
                // It seems to need to be on the UI to always receive changes
                addHiddenProgressIndicator(progressIndicator);


                // Ensure the taskDisplay is listening for the task state to instigate display updates
                for (final KOSTaskDisplay taskDisplay : taskDisplays) {
                    final ChangeListener<Worker.State> stateListener = new ChangeListener<Worker.State>() {
                        @Override
                        public void changed(ObservableValue<? extends Worker.State> observable,
                                            Worker.State oldValue, Worker.State newValue) {
                            taskDisplay.updateGraphicFromState();
                        }
                    };
                    taskDisplay.getTask().stateProperty().addListener(stateListener);
                    taskDisplay.getTask().setOnSucceeded(new EventHandler<WorkerStateEvent>() {
                        @Override
                        public void handle(WorkerStateEvent workerStateEvent) {
                            if (EntityStatus.isKOS(taskDisplay.getTask().getKOSStatusSummary().getResolvedStatus())) {
                                playKOSSoundIfRequired(false);
                            }
                        }
                    });
                }
                for (final KOSTaskDisplay taskDisplay : taskDisplays) {
                    taskDisplay.updateGraphicFromState();
                    m_executorService.execute(taskDisplay.getTask());
                }
                m_resultsList.addAll(0, taskDisplays);
            }
        });
    }

    private ChangeListener<Number> getSmallBatchProgressChangeListener(final Set<KOSTaskDisplay> taskDisplays,
                                                                       final ProgressBar progressIndicator) {
        return new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldNumber,
                                Number newNumber) {
                m_logger.debug("Batch Progress : oldNumber=" + oldNumber + ", newNumber=" + newNumber);
                if ((Math.round(newNumber.doubleValue() * 1000.0) / 1000.0) >= 1.0) {
                    if (m_logger.isDebugEnabled()) {
                        m_logger.debug("Finished Small Batch : " + getBatchStringFromDisplays(taskDisplays));
                    }
                    // When Tasks are finished, fade out their displays
                    for (KOSTaskDisplay display : taskDisplays) {
                        if (m_processingLargeBatch.get()
                                && m_largeBatch.contains(display.getTask())
                                && EntityStatus.isKOS(display.getTask().getKOSStatusSummary().getResolvedStatus())) {
                            // Don't fade tasks assimilated by a large batch
                            if (m_logger.isDebugEnabled()) {
                                m_logger.debug("Not fading '" + display.getTask().getPilotName() + "' due to Large Batch.");
                            }
                        }
                        else if (m_holding) {
                            if (m_logger.isDebugEnabled()) {
                                m_logger.debug("Not fading '" + display.getTask().getPilotName() + "' due to global hold.");
                            }
                        }
                        else {
                            display.fadeOut(false);
                        }
                    }
                    m_overlayController.getResultPanelFooter().getChildren().remove(progressIndicator);
                }
            }
        };
    }

    private void addHiddenProgressIndicator(final ProgressBar progressIndicator) {
        Pane footer = m_overlayController.getResultPanelFooter();
        footer.getChildren().add(progressIndicator);
        progressIndicator.setMaxHeight(0);
        progressIndicator.setMaxWidth(0);
        footer.setMaxWidth(0);
        footer.setMaxHeight(0);
        footer.setVisible(false);
    }

    private String getBatchStringFromDisplays(Collection<KOSTaskDisplay> taskDisplays) {
        StringBuilder batchString = new StringBuilder(30);
        for (KOSTaskDisplay taskDisplay : taskDisplays) {
            if (batchString.length() > 0) {
                batchString.append(", ");
            }
            batchString.append(taskDisplay.getTask());
        }
        return batchString.toString();
    }

    @Override
    public synchronized void setHolding(final boolean holding) {
        if (holding != m_holding) {
            m_holding = holding;
            Platform.runLater(new Runnable() {
                public void run() {
                    if (!holding) {
                        if (!m_processingLargeBatch.get()) {
                            for (KOSTaskDisplay display : m_resultsList) {
                                if (display.getTask().isDone()) {
                                    display.fadeOut(true);
                                }
                            }
                        }
                    }
                    else if (m_resultsList.size() == 0) {
                        for (KOSTaskDisplay display : m_lastResults) {
                            display.resetFade();
                        }
                        m_resultsList.addAll(m_lastResults);
                    }
                    else {
                        for (KOSTaskDisplay display : m_resultsList) {
                            display.stopFadingIfNotStarted();
                        }
                    }
                }
            });
        }
    }

    private void playKOSSoundIfRequired(boolean largeBatch) {
        if (Boolean.valueOf(m_configManager.getValue(ConfigKeyConstants.PLAY_KOS_SOUND, ConfigKeyConstants.PLAY_KOS_SOUND_DEFAULT))
                && (!largeBatch
                || !Boolean.valueOf(m_configManager.getValue(ConfigKeyConstants.PLAY_KOS_SOUND_ONCE, ConfigKeyConstants.PLAY_KOS_SOUND_ONCE_DEFAULT))
                || m_kosSoundPlayedForBatch.compareAndSet(false, true))) {
            m_logger.debug("Play KOS Sound");
            //PLAY SOUND
            if (CUSTOM_KOS_SOUND != null) {
                CUSTOM_KOS_SOUND.play();
            }
            else {
                DEFAULT_KOS_SOUND.play();
            }
        }
    }

    @Override
    public void notifyConfigChange() {
        m_logger.debug("Notified of config change");
        initialise();
    }
}
