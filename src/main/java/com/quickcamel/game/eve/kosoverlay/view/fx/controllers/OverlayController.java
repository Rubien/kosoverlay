/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.view.fx.controllers;

import com.beimin.eveapi.corporation.sheet.CorpSheetResponse;
import com.beimin.eveapi.eve.character.CharacterInfoResponse;
import com.google.common.cache.LoadingCache;
import com.quickcamel.game.eve.kosoverlay.initiator.KeyListener;
import com.quickcamel.game.eve.kosoverlay.service.dto.*;
import com.quickcamel.game.eve.kosoverlay.view.AlwaysOnTopDialogue;
import com.quickcamel.game.eve.kosoverlay.view.fx.UIConstants;
import com.quickcamel.game.eve.kosoverlay.configuration.ConfigKeyConstants;
import com.quickcamel.game.eve.kosoverlay.configuration.IConfigManager;
import com.quickcamel.game.eve.kosoverlay.service.IKOSBatchService;
import com.quickcamel.game.eve.kosoverlay.view.ComponentMover;
import com.quickcamel.game.eve.kosoverlay.view.fx.KOSTaskDisplay;
import javafx.animation.*;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.util.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.inject.Inject;

/**
 * JavaFX Controller for the Overlay Parent
 *
 * @author Louis Burton
 */
public class OverlayController {

    private static final Logger m_logger = LoggerFactory.getLogger(OverlayController.class);

    @FXML
    private HBox m_root;

    @FXML
    private ToggleButton m_anchorButton;

    @FXML
    private VBox m_resultPanel;

    @FXML
    private HBox m_resultPanelFooter;

    @FXML
    private ListView<KOSTaskDisplay> m_resultView;

    @FXML
    private MenuItem m_menuItemAnchor;
    private final BooleanProperty m_anchored = new SimpleBooleanProperty(false);

    @FXML
    private MenuItem m_menuItemHelp;
    private final BooleanProperty m_helpShowing = new SimpleBooleanProperty(true);

    @FXML
    private HBox m_helpBox;

    @FXML
    private HBox m_settings;

    @Resource(name = "resultsList")
    private ObservableList<KOSTaskDisplay> m_resultsList;

    @Inject
    private IKOSBatchService m_kosBatchService;

    @Inject
    private ComponentMover m_componentMover;

    @Inject
    private IConfigManager m_configManager;

    @Inject
    private AlwaysOnTopDialogue m_alwaysOnTopDialogue;

    @Inject
    private KeyListener m_keyListener;

    @Resource(name = "corpCVACache")
    private LoadingCache<String, KOSResultContainerDTO> m_corpCVACache;

    @Resource(name = "allianceCVACache")
    private LoadingCache<String, KOSResultContainerDTO> m_allianceCVACache;

    @Resource(name = "idToNameCache")
    private LoadingCache<Long, String> m_idToNameCache;

    @Resource(name = "idToCharacterInfoCache")
    private LoadingCache<Long, CharacterInfoResponse> m_idToCharacterInfoCache;

    @Resource(name = "pilotCVACache")
    private LoadingCache<String, KOSResultContainerDTO> m_pilotCVACache;

    @Resource(name = "nameToIdCache")
    private LoadingCache<String, Long> m_nameToIdCache;

    @Resource(name = "corpIdToCorpSheetCache")
    private LoadingCache<Long, CorpSheetResponse> m_corpIdToCorpSheetCache;

    private FadeTransition m_helpFadeTransition;

    @FXML
    private void initialize() {
        m_helpFadeTransition = new FadeTransition(Duration.millis(UIConstants.FADE_TRANSITION_DURATION), m_helpBox);
        m_helpFadeTransition.setToValue(0.0);
        m_helpFadeTransition.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                m_logger.debug("Help fading finished");
                removeHelp(false);
                m_helpFadeTransition.jumpTo(Duration.ZERO);
            }
        });
        m_resultView.setItems(m_resultsList);
        m_componentMover.registerAnchorButton(m_anchorButton);
        m_anchored.setValue(Boolean.valueOf(m_configManager.getValue(ConfigKeyConstants.ANCHORED, "false")));
        m_componentMover.setAnchored(m_anchored.get());
        m_menuItemAnchor.textProperty().bind(Bindings
                .when(m_anchored)
                .then("Unlock Anchor")
                .otherwise("Lock Anchor"));
        boolean helpShown = Boolean.valueOf(m_configManager.getValue(ConfigKeyConstants.HELP_SHOWN, "false"));
        if (helpShown) {
            removeHelp(false);
            m_helpShowing.setValue(false);
        }
        m_menuItemHelp.textProperty().bind(Bindings
                .when(m_helpShowing)
                .then("Hide Help")
                .otherwise("Show Help"));
        removeSettings();
    }

    public void handleButtonAction() {
        m_logger.debug("HOLD");
        m_kosBatchService.setHolding(m_anchorButton.isSelected());
    }

    public Pane getResultPanel() {
        return m_resultPanel;
    }

    public HBox getResultPanelFooter() {
        return m_resultPanelFooter;
    }

    public void switchAnchor() {
        m_anchored.setValue(!m_anchored.get());
        m_configManager.setValue(ConfigKeyConstants.ANCHORED, m_anchored.getValue().toString());
        m_componentMover.setAnchored(m_anchored.get());
    }

    public void exit() {
        Platform.exit();
    }

    public void removeHelp(boolean fade) {
        if (m_helpShowing.get()) {
            m_logger.debug("Removing help - fade=" + fade);
            m_configManager.setValue(ConfigKeyConstants.HELP_SHOWN, "true");
            if (fade) {
                m_helpFadeTransition.play();
            }
            else {
                m_root.getChildren().remove(m_helpBox);
                m_helpShowing.setValue(false);
                m_resultPanel.setVisible(true);
            }
        }
    }

    public void showHelp() {
        m_helpBox.setOpacity(1.00);
        m_resultPanel.setVisible(false);
        removeSettings();
        m_root.getChildren().add(1, m_helpBox);
        m_helpShowing.setValue(true);
    }

    public synchronized void switchHelp() {
        if (m_helpShowing.get()) {
            removeHelp(false);
        }
        else {
            showHelp();
        }
    }

    public void showSettings() {
        if (!m_root.getChildren().contains(m_settings)) {
            removeHelp(false);
            m_resultPanel.setVisible(false);
            m_root.getChildren().add(1, m_settings);
            m_alwaysOnTopDialogue.setFocusableWindowState(true);
            m_alwaysOnTopDialogue.setSettingsShowing(true);
        }

    }

    public void removeSettings() {
        if (m_root.getChildren().contains(m_settings)) {
            m_alwaysOnTopDialogue.setFocusableWindowState(false);
            m_alwaysOnTopDialogue.setSettingsShowing(false);
            m_root.getChildren().remove(m_settings);
            m_resultPanel.setVisible(true);
        }
    }

    public void removeSidePanels() {
        removeHelp(false);
        removeSettings();
    }

    public void runFromClipboard() {
        m_keyListener.runFromClipboard();
    }

    public void clearCache() {
        m_allianceCVACache.invalidateAll();
        m_corpCVACache.invalidateAll();
        m_pilotCVACache.invalidateAll();
        m_idToNameCache.invalidateAll();
        m_nameToIdCache.invalidateAll();
        m_idToCharacterInfoCache.invalidateAll();
        m_corpIdToCorpSheetCache.invalidateAll();
    }
}
