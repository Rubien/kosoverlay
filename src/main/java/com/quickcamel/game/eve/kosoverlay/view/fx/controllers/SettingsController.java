/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.view.fx.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

/**
 * JavaFX Controller for the Settings pane
 *
 * @author Louis Burton
 */
public class SettingsController {

    private static final Logger m_logger = LoggerFactory.getLogger(SettingsController.class);

    @FXML
    private Label m_error;

    @FXML
    private TabPane m_settings;

    @FXML
    private SettingsTabGeneralController m_settingsGeneralController;

    @FXML
    private SettingsTabStandingsController m_settingsStandingsController;

    @FXML
    private SettingsTabSoundsKBController m_settingsSoundsKBController;

    private ISettingsTabController[] m_allSettingsTabControllers;

    @Inject
    private OverlayController m_overlayController;


    @FXML
    public void initialize() {
        m_allSettingsTabControllers = new ISettingsTabController[]
                {m_settingsGeneralController, m_settingsStandingsController, m_settingsSoundsKBController};
        reset();
    }

    private void reset() {
        m_logger.debug("Resetting");
        m_error.setText("");
        for (ISettingsTabController settingsTabController : m_allSettingsTabControllers) {
            settingsTabController.initialize();
        }
    }

    public void save() {
        boolean failedValidation = false;
        for (int i = 0; i < m_allSettingsTabControllers.length && !failedValidation; ++i) {
            String errorText;
            try {
                errorText = m_allSettingsTabControllers[i].validate();
            }
            catch (Throwable e) {
                errorText = "The settings provided were not successfully validated.";
            }
            if (errorText != null) {
                m_error.setText(errorText);
                m_settings.getSelectionModel().select(i);
                failedValidation = true;
                m_logger.debug("Settings failed validation - " + m_error.getText());
            }
        }
        if (!failedValidation) {
            m_logger.debug("Validated settings");
            for (ISettingsTabController settingsTabController : m_allSettingsTabControllers) {
                settingsTabController.save();
            }
            m_overlayController.removeSettings();
            reset();
        }
    }

    public void cancel() {
        m_overlayController.removeSettings();
        reset();
    }
}
