/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.view.fx.controllers;

import com.quickcamel.game.eve.kosoverlay.configuration.ConfigKeyConstants;
import com.quickcamel.game.eve.kosoverlay.configuration.IConfigChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

/**
 * Controller functionality for the settings tab - orientated around general considerations
 *
 * @author Louis Burton
 */
public class SettingsTabGeneralController extends SettingsTabController {

    private static final Logger m_logger = LoggerFactory.getLogger(SettingsTabGeneralController.class);

    public static final String MAXIMUM_DISPLAY_TOOLTIP = "The limit for the number of results that can be shown.";
    public static final String LARGE_BATCH_THRESHOLD_TOOLTIP = "The threshold for when to use large batch handling.\n" +
            "Must be lower than the Maximum Display, as Small Batches display all checks from the outset.";
    public static final String BRING_TO_FRONT_TOOLTIP = "Keeping a window on top can depend on your environment and other applications.\n" +
            "Turn this on if you need the KOS Overlay to aggressively bring itself to the front periodically.";
    public static final String FADE_TIMEOUT_TOOLTIP = "The amount of seconds waited once a check, or batch of checks, completes before fading out the results.";

    @FXML
    private TextField m_largeBatchThreshold;

    @FXML
    private TextField m_resultsMaximum;

    @FXML
    private CheckBox m_bringToFront;

    @FXML
    private TextField m_fadeTimeout;

    @Resource(name = "kosBatchService")
    private IConfigChangeListener m_batchServiceConfigListener;

    @Resource(name = "alwaysOnTopDialogue")
    private IConfigChangeListener m_alwaysOnTopDialogue;

    @FXML
    public void initialize() {
        m_logger.debug("Initialising");
        m_largeBatchThreshold.setText(m_configManager.getValue(ConfigKeyConstants.BATCHSERVICE_LARGEBATCH_THRESHOLD, ConfigKeyConstants.BATCHSERVICE_LARGEBATCH_THRESHOLD_DEFAULT));
        m_resultsMaximum.setText(m_configManager.getValue(ConfigKeyConstants.RESULTS_MAXIMUM, ConfigKeyConstants.RESULTS_MAXIMUM_DEFAULT));
        m_fadeTimeout.setText(String.valueOf(Double.valueOf(m_configManager.getValue(ConfigKeyConstants.FADE_PAUSE_DURATION, ConfigKeyConstants.FADE_PAUSE_DURATION_DEFAULT)) / 1000));
        m_bringToFront.setSelected(Integer.parseInt(m_configManager.getValue(ConfigKeyConstants.OVERLAY_ALWAYSONTOP_DELAY_MS, ConfigKeyConstants.OVERLAY_ALWAYSONTOP_DELAY_MS_DEFAULT)) > 0);
    }

    public String validate() {
        String errorText = null;
        int resultsMax = Integer.parseInt(m_configManager.getValue(ConfigKeyConstants.RESULTS_MAXIMUM, ConfigKeyConstants.RESULTS_MAXIMUM_DEFAULT));
        int largeBatchThreshold = Integer.parseInt(m_configManager.getValue(ConfigKeyConstants.BATCHSERVICE_LARGEBATCH_THRESHOLD, ConfigKeyConstants.BATCHSERVICE_LARGEBATCH_THRESHOLD_DEFAULT));
        Double.parseDouble(m_fadeTimeout.getText());
        if (m_resultsMaximum.getText() != null && m_resultsMaximum.getText().trim().length() > 0) {
            resultsMax = Integer.parseInt(m_resultsMaximum.getText().trim());
        }
        if (m_largeBatchThreshold.getText() != null && m_largeBatchThreshold.getText().trim().length() > 0) {
            largeBatchThreshold = Integer.parseInt(m_largeBatchThreshold.getText().trim());
        }
        if (largeBatchThreshold > resultsMax) {
            errorText = "The Large Batch Threshold can't be higher than the Maximum Display. Small Batches display checks from the outset.";
        }
        return errorText;
    }

    public void save() {
        m_logger.debug("Saving settings");
        boolean needsBatchServiceUpdate = updateValueIfSet(ConfigKeyConstants.RESULTS_MAXIMUM, m_resultsMaximum.getText());
        needsBatchServiceUpdate = (updateValueIfSet(ConfigKeyConstants.BATCHSERVICE_LARGEBATCH_THRESHOLD, m_largeBatchThreshold.getText()) || needsBatchServiceUpdate);
        needsBatchServiceUpdate = (updateValueIfSet(ConfigKeyConstants.FADE_PAUSE_DURATION, String.valueOf((long) (Double.valueOf(m_fadeTimeout.getText()) * 1000))) || needsBatchServiceUpdate);

        boolean needsAlwaysOnTopUpdate;
        if (!m_bringToFront.isSelected()) {
            needsAlwaysOnTopUpdate = updateValueIfSet(ConfigKeyConstants.OVERLAY_ALWAYSONTOP_DELAY_MS, "-1");
        }
        else {
            String existingAlwaysOnTopString = m_configManager.getValue(ConfigKeyConstants.OVERLAY_ALWAYSONTOP_DELAY_MS);
            if (existingAlwaysOnTopString == null || Integer.parseInt(existingAlwaysOnTopString) <= 0) {
                existingAlwaysOnTopString = "5000";
            }
            needsAlwaysOnTopUpdate = updateValueIfSet(ConfigKeyConstants.OVERLAY_ALWAYSONTOP_DELAY_MS, existingAlwaysOnTopString);
        }
        initialize();
        // TODO - clever auto registration of listeners to properties would be nice
        if (needsBatchServiceUpdate) {
            m_batchServiceConfigListener.notifyConfigChange();
        }
        if (needsAlwaysOnTopUpdate) {
            m_alwaysOnTopDialogue.notifyConfigChange();
        }
    }
}