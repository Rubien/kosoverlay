/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.view.fx;

import com.quickcamel.game.eve.kosoverlay.configuration.ConfigKeyConstants;
import com.quickcamel.game.eve.kosoverlay.configuration.IConfigManager;
import com.quickcamel.game.eve.kosoverlay.service.*;
import com.quickcamel.game.eve.kosoverlay.service.tasks.*;
import com.quickcamel.game.eve.kosoverlay.service.dto.KOSPilotResultDTO;
import javafx.animation.*;
import javafx.collections.ObservableList;
import javafx.concurrent.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.util.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Inject;
import java.io.IOException;
import java.net.URI;

/**
 * A display of the KOSCheckTask
 *
 * @author Louis Burton
 */
public class KOSTaskDisplay extends Region {

    private static final Logger m_logger = LoggerFactory.getLogger(KOSTaskDisplay.class);

    private final ImageView m_loadingImageView = new ImageView(UIConstants.LOADING_IMAGE);
    private final ImageView m_errorImageView = new ImageView(UIConstants.ERROR_IMAGE);

    @Resource(name = "resultsList")
    private ObservableList<KOSTaskDisplay> m_resultsList;

    @Inject
    private IPilotInfoURLBuilder m_pilotInfoURLBuilder;

    @Inject
    private IConfigManager m_configManager;

    // PARENT
    private final VBox m_parent = new VBox();

    // TOP ROW
    private final HBox m_hBoxTopRow = new HBox();
    private final HBox m_hBoxButtonWrapper = new HBox();
    private final ToggleButton m_button = new ToggleButton();
    private final Text m_pilotText = new Text();
    private final HBox m_shortSummaryWrapper = new HBox();
    private final Text m_shortSummary = new Text();

    // EXPANDABLE
    private final GridPane m_hBoxBottomRow = new GridPane();
    private final ImageView m_infoImageView = new ImageView(UIConstants.INFO_IMAGE);
    private final ImageView m_pilotImageView = new ImageView();
    private final ImageView m_corpImageView = new ImageView();
    private final ImageView m_allianceImageView = new ImageView();
    private final Text m_pilotTextExpandable = new Text();
    private final Text m_corpTextExpandable = new Text();
    private final Text m_allianceTextExpandable = new Text();

    private boolean m_expanded = false;
    private boolean m_receivedFade = false;
    private long m_fadePauseDuration;

    // The Task this instance displays
    private KOSCheckTask m_task;

    private Transition m_transition;

    public KOSTaskDisplay() {
        super();
    }

    @PostConstruct
    private void initialise() {
        m_fadePauseDuration = Long.valueOf(m_configManager.getValue(ConfigKeyConstants.FADE_PAUSE_DURATION, ConfigKeyConstants.FADE_PAUSE_DURATION_DEFAULT));
        // Until Java8 is used, don't use FXML due to cost of reflection
        initView();
        initFadeTransition();
    }

    /**
     * Initialise the JFX view of the KOSTaskDisplay
     * Until Java8 is used, don't use FXML due to cost of reflection
     */
    private void initView() {
        this.setMinWidth(450);
        this.setMaxWidth(450);
        this.setPrefWidth(450);
        m_hBoxButtonWrapper.setPadding(new Insets(0, 0, 0, 0));
        m_hBoxButtonWrapper.setMinWidth(30);
        m_hBoxButtonWrapper.setMaxWidth(30);
        m_hBoxButtonWrapper.setPrefWidth(30);
        m_hBoxButtonWrapper.setAlignment(Pos.CENTER);
        m_hBoxButtonWrapper.getChildren().addAll(m_loadingImageView);

        m_hBoxTopRow.setPadding(new Insets(0, 0, 0, 0));
        m_hBoxTopRow.setMinHeight(18);
        m_hBoxTopRow.setMaxHeight(18);
        m_hBoxTopRow.setPrefHeight(18);
        m_hBoxTopRow.setAlignment(Pos.CENTER_LEFT);

        HBox pilotTextClipBox = new HBox();
        pilotTextClipBox.setPadding(new Insets(0, 0, 0, 0));
        pilotTextClipBox.setMaxWidth(310);
        pilotTextClipBox.setMinWidth(10);
        Rectangle rectangle = new Rectangle(313, 25);
        rectangle.setLayoutX(-3);
        rectangle.setLayoutY(-3);
        pilotTextClipBox.setClip(rectangle);
        pilotTextClipBox.setAlignment(Pos.CENTER_LEFT);
        pilotTextClipBox.getChildren().addAll(m_pilotText);
        m_hBoxTopRow.getChildren().addAll(m_hBoxButtonWrapper, pilotTextClipBox);

        m_button.setAlignment(Pos.CENTER);
        m_button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (m_expanded) {
                    m_parent.getChildren().remove(m_parent.getChildren().size() - 1);
                    if (m_receivedFade) {
                        fadeOut(true);
                    }
                }
                else {
                    if (m_transition.getCurrentTime().greaterThan(Duration.ZERO)) {
                        m_transition.stop();
                        m_transition.jumpTo(Duration.ZERO);
                        KOSTaskDisplay.this.setOpacity(1.00);
                    }
                    m_parent.getChildren().add(m_hBoxBottomRow);
                }
                m_expanded = !m_expanded;
            }
        });
        m_button.setMaxWidth(17);
        m_button.setMinWidth(17);
        m_button.setMaxHeight(17);
        m_button.setMinHeight(17);

        m_shortSummaryWrapper.setMinWidth(USE_PREF_SIZE);
        m_shortSummaryWrapper.setPadding(new Insets(0, 0, 0, 8));
        m_shortSummaryWrapper.setAlignment(Pos.CENTER_RIGHT);
        m_shortSummaryWrapper.getChildren().add(m_shortSummary);
        m_shortSummary.getStyleClass().addAll(UIConstants.BASE_TEXT_STYLE);
        m_pilotText.getStyleClass().add(UIConstants.BASE_TEXT_STYLE);

        m_parent.getChildren().add(m_hBoxTopRow);

        initExpandable();
        // If graphic hasn't been set - set it
        getChildren().add(m_parent);
    }

    protected void initExpandable() {
        m_infoImageView.setFitHeight(16);
        m_infoImageView.setFitWidth(16);
        m_infoImageView.setVisible(false);
        m_pilotImageView.setFitHeight(16);
        m_pilotImageView.setFitWidth(16);
        m_corpImageView.setFitHeight(16);
        m_corpImageView.setFitWidth(16);
        m_allianceImageView.setFitHeight(16);
        m_allianceImageView.setFitWidth(16);
        m_hBoxBottomRow.setHgap(5);
        m_hBoxBottomRow.setVgap(5);
        m_hBoxBottomRow.setPadding(new Insets(4, 0, 0, 25));
        m_hBoxBottomRow.add(m_infoImageView, 0, 0);
        m_hBoxBottomRow.add(m_pilotImageView, 1, 0);
        m_hBoxBottomRow.add(m_corpImageView, 1, 1);
        m_hBoxBottomRow.add(m_allianceImageView, 1, 2);
        m_hBoxBottomRow.add(m_pilotTextExpandable, 2, 0);
        m_hBoxBottomRow.add(m_corpTextExpandable, 2, 1);
        m_hBoxBottomRow.add(m_allianceTextExpandable, 2, 2);
        m_pilotTextExpandable.getStyleClass().add(UIConstants.BASE_TEXT_STYLE);
        m_corpTextExpandable.getStyleClass().add(UIConstants.BASE_TEXT_STYLE);
        m_allianceTextExpandable.getStyleClass().add(UIConstants.BASE_TEXT_STYLE);
    }

    protected void updateExpandable(final KOSCheckTask task) {
        new Thread(new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                final KOSPilotResultDTO pilot = task.getPilotKOSDetails();
                if (pilot == null) {
                    m_pilotTextExpandable.setText("Not Found");
                    m_pilotTextExpandable.getStyleClass().add(UIConstants.LOADING_STYLE);
                    m_hBoxBottomRow.getChildren().clear();
                    m_hBoxBottomRow.add(m_infoImageView, 0, 0);
                    m_hBoxBottomRow.add(m_pilotTextExpandable, 1, 0);
                }
                else {
                    KOSStatusSummary summary = task.getKOSStatusSummary();
                    m_pilotTextExpandable.setText(pilot.getLabel());
                    if (pilot.getIcon() != null) {
                        m_pilotImageView.setImage(resolveImage(pilot.getIcon()));
                    }
                    updateDisplayForKOSStatus(summary.getPilotStatus(), m_pilotTextExpandable, null);
                    if (pilot.getCorp() == null) {
                        m_hBoxBottomRow.getChildren().remove(m_corpImageView);
                        m_hBoxBottomRow.getChildren().remove(m_corpTextExpandable);
                        m_hBoxBottomRow.getChildren().remove(m_allianceImageView);
                        m_hBoxBottomRow.getChildren().remove(m_allianceTextExpandable);
                    }
                    else {
                        m_corpTextExpandable.setText(pilot.getCorp().getLabel());
                        if (pilot.getCorp().getIcon() != null) {
                            m_corpImageView.setImage(resolveImage(pilot.getCorp().getIcon()));
                        }
                        updateDisplayForKOSStatus(summary.getCorpStatus(), m_corpTextExpandable, null);
                        if (pilot.getCorp().getAlliance() == null) {
                            m_hBoxBottomRow.getChildren().remove(m_allianceImageView);
                            m_hBoxBottomRow.getChildren().remove(m_allianceTextExpandable);
                        }
                        else {
                            m_allianceTextExpandable.setText(pilot.getCorp().getAlliance().getLabel());
                            if (pilot.getCorp().getAlliance().getIcon() != null) {
                                m_allianceImageView.setImage(resolveImage(pilot.getCorp().getAlliance().getIcon()));
                            }
                            updateDisplayForKOSStatus(summary.getAllianceStatus(), m_allianceTextExpandable, null);
                        }
                    }
                    // Do last as is potentially expensive
                    final URI url = m_pilotInfoURLBuilder.getURL(pilot);
                    if (url == null) {
                        m_hBoxBottomRow.getChildren().remove(m_infoImageView);
                    }
                    else {
                        m_infoImageView.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent mouseEvent) {
                                try {
                                    java.awt.Desktop.getDesktop().browse(url);
                                }
                                catch (IOException e) {
                                    m_logger.error("Unable to open browser to url :" + url.toString(), e);
                                }
                            }
                        });
                        m_infoImageView.setVisible(true);
                    }
                }
                return null;
            }
        }).start();
    }

    private Image resolveImage(String url) {
        // TODO - these are actually cached on the FS by Eve - using this when possible would be preferable (or at least caching!)
        Image image;
        if (url.equals("error.png")) {
            image = UIConstants.ERROR_IMAGE;
        }
        else {
            image = new Image(url);
        }
        return image;
    }

    public void updateGraphicFromState() {
        final KOSCheckTask task = m_task;
        final Worker.State state = task.getState();
        if (m_logger.isDebugEnabled()) {
            m_logger.debug("ENTER - task=" + m_task + ", pilot=" + task.getPilotName() + ", text=" + m_pilotText.toString() + ", textValue=" + m_pilotText.getText() + ", state=" + state);
        }
        // If the pilotName has changed, update it
        if (!task.getPilotName().equals(m_pilotText.getText())) {
            if (m_logger.isDebugEnabled()) {
                m_logger.debug("RENDERING - update text for task=" + m_task + ", pilot=" + task.getPilotName());
            }
            m_pilotText.setText(task.getPilotName());
        }

        // Act on Task states
        if (state == Worker.State.READY || state == Worker.State.SCHEDULED || state == Worker.State.RUNNING) {
            if (!m_pilotText.getStyleClass().contains(UIConstants.LOADING_STYLE)) {
                m_pilotText.getStyleClass().add(UIConstants.LOADING_STYLE);
                m_hBoxButtonWrapper.getChildren().set(0, m_loadingImageView);
            }
        }
        else if (state == Worker.State.FAILED || state == Worker.State.CANCELLED) {
            m_pilotText.getStyleClass().remove(UIConstants.LOADING_STYLE);
            m_pilotText.getStyleClass().add(UIConstants.FAILED_STYLE);
            m_hBoxButtonWrapper.getChildren().set(0, m_errorImageView);
        }
        else if (state == Worker.State.SUCCEEDED) {
            m_pilotText.getStyleClass().remove(UIConstants.LOADING_STYLE);
            EntityStatus status = task.getKOSStatusSummary().getResolvedStatus();
            updateDisplayForKOSStatus(status, m_pilotText, m_button);
            if (status.equals(EntityStatus.ERROR)) {
                m_hBoxButtonWrapper.getChildren().set(0, m_errorImageView);
            }
            else {
                if (m_logger.isDebugEnabled()) {
                    m_logger.debug("Success result for pilot " + task.getPilotName() + " is " + status);
                }
                updateExpandable(task);
                m_hBoxButtonWrapper.getChildren().set(0, m_button);
                if (EntityStatus.isKOS(status)) {
                    m_shortSummary.getStyleClass().add(UIConstants.KOS_STYLE);
                    m_shortSummary.getStyleClass().add(UIConstants.RED_STYLE);
                    m_shortSummary.setText(task.getKOSStatusSummary().getShortReason());
                    m_shortSummary.setStyle("-fx-font: bold 12pt \"Verdana\"");
                    m_hBoxTopRow.getChildren().add(m_shortSummaryWrapper);
                }
            }
            if (task.getKOSStatusSummary().isWarning()) {
                m_button.setText("!");
            }
        }
        else if (m_logger.isDebugEnabled()) {
            m_logger.debug("RENDERING - no further changes for task=" + m_task + ", pilot=" + task.getPilotName());
        }

        if (m_logger.isDebugEnabled()) {
            m_logger.debug("EXIT - task=" + m_task + ", pilot=" + task.getPilotName() + ", text=" + m_pilotText.toString() + ", textValue=" + m_pilotText.getText() + ", state=" + state);
        }
    }

    private void initFadeTransition() {
        final PauseTransition pauseTransition = new PauseTransition(Duration.millis(m_fadePauseDuration));
        final FadeTransition fadeTransition = new FadeTransition(Duration.millis(UIConstants.FADE_TRANSITION_DURATION), this);
        fadeTransition.setFromValue(0.85);
        fadeTransition.setToValue(0.0);
        m_transition = new SequentialTransition(pauseTransition, fadeTransition);
        m_transition.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                m_logger.debug("Fading finished - task=" + m_task + ", currentDuration=" + m_transition.currentTimeProperty().get() + ", totalDuration=" + m_transition.getTotalDuration());
                m_resultsList.remove(KOSTaskDisplay.this);
            }
        });
    }

    private void updateDisplayForKOSStatus(EntityStatus status, Text text, ToggleButton button) {
        ObservableList<String> styles = text.getStyleClass();
        styles.removeAll(UIConstants.OVERLAY_STYLES);
        switch (status) {
            case UNKNOWN:
                styles.add(UIConstants.LOADING_STYLE);
                break;
            case NEUTRAL:
                styles.add(UIConstants.NEUTRAL_STYLE);
                if (button != null) {
                    button.setText("=");
                    button.setStyle("-fx-color: grey");
                }
                break;
            case KOS_CVA:
                styles.add(UIConstants.KOS_STYLE);
                styles.add(UIConstants.RED_STYLE);
                if (button != null) {
                    button.setText("−");
                    button.setStyle("-fx-color: red");
                }
                break;
            case KOS_CVA_CONFLICT:
                styles.add(UIConstants.KOS_STYLE);
                styles.add(UIConstants.ORANGE_STYLE);
                if (button != null) {
                    button.setText("−");
                    button.setStyle("-fx-color: orange");
                }
                break;
            case RED_10:
                styles.add(UIConstants.RED_STYLE);
                styles.add(UIConstants.STANDINGS_STYLE);
                if (button != null) {
                    button.setText("−");
                    button.setStyle("-fx-color: red");
                }
                break;
            case RED_5:
                styles.add(UIConstants.ORANGE_STYLE);
                styles.add(UIConstants.STANDINGS_STYLE);
                if (button != null) {
                    button.setText("−");
                    button.setStyle("-fx-color: orange");
                }
                break;
            case BLUE_SAME_ALLIANCE:
                styles.add(UIConstants.BLUE_STYLE);
                if (button != null) {
                    button.setText("✰");
                    button.setTextFill(Color.WHITE);
                    button.setStyle("-fx-color: #006dff; fx-font-color: white;");
                }
                break;
            case BLUE_10:
                styles.add(UIConstants.BLUE_STYLE);
                if (button != null) {
                    button.setText("+");
                    button.setStyle("-fx-color: #006dff");
                }
                break;
            case BLUE_5:
                styles.add(UIConstants.LIGHT_BLUE_STYLE);
                if (button != null) {
                    button.setText("+");
                    button.setStyle("-fx-color: #00bbff");
                }
                break;
            case BLUE_SAME_PILOT_OR_CORP:
                styles.add(UIConstants.GREEN_STYLE);
                if (button != null) {
                    button.setText("✰");
                    button.setTextFill(Color.WHITE);
                    button.setStyle("-fx-color: #00c400; fx-font-color: white;");
                }
                break;
            case NEUTRAL_STANDINGS:
                styles.add(UIConstants.NEUTRAL_STANDINGS_STYLE);
                styles.add(UIConstants.STANDINGS_STYLE);
                if (button != null) {
                    button.setText("=");
                    button.setStyle("-fx-color: #e6e6e6");
                }
                break;
            case ERROR:
                styles.add(UIConstants.FAILED_STYLE);
        }
    }

    public boolean stopFadingIfNotStarted() {
        boolean result;
        if (m_transition.getCurrentTime().greaterThan(Duration.millis(m_fadePauseDuration - 200))) {
            result = false;
        }
        else {
            m_transition.stop();
            m_transition.jumpTo(Duration.ZERO);
            m_receivedFade = false;
            result = true;
        }
        return result;
    }

    public void fadeOut(boolean skipPause) {
        m_receivedFade = true;
        if (m_button.isSelected()) {
            if (m_logger.isDebugEnabled()) {
                m_logger.debug("Details showing, not fading - task=" + m_task + ", pilot=" + m_task.getPilotName());
            }
        }
        else {
            // Don't double fade
            if (m_transition.getCurrentTime().greaterThan(Duration.ZERO)) {
                if (m_logger.isDebugEnabled()) {
                    m_logger.debug("Already fading, not fading - task=" + m_task + ", pilot=" + m_task.getPilotName());
                }
            }
            else {
                if (skipPause) {
                    m_transition.playFrom(Duration.millis(m_fadePauseDuration));
                }
                else {
                    m_transition.playFromStart();
                }
            }
        }
    }

    public void resetFade() {
        if (m_logger.isDebugEnabled()) {
            m_logger.debug("Resetting fade - task=" + m_task + ", pilot=" + m_task.getPilotName());
        }
        if (m_transition.getCurrentTime().greaterThan(Duration.ZERO)) {
            m_transition.stop();
            m_transition.jumpTo(Duration.ZERO);
            KOSTaskDisplay.this.setOpacity(1.00);
        }
        m_receivedFade = false;
    }

    public KOSCheckTask getTask() {
        return m_task;
    }

    public void setTask(KOSCheckTask task) {
        m_task = task;
    }
}