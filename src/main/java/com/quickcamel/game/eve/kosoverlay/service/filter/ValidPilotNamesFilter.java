/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.regex.Pattern;

/**
 * Filter down to well formatted EVE pilot names (accepted characters)
 *
 * @author Louis Burton
 */
public class ValidPilotNamesFilter implements IPilotFilter {

    private static final Logger m_logger = LoggerFactory.getLogger(ValidPilotNamesFilter.class);
    // Until a definitive list of characters are found - don't filter in a strict manner
    // private static final Pattern PATTERN = Pattern.compile("^[a-zA-Z0-9 \\.'-ō]+$");

    @Override
    public Set<String> getFilteredPilots(Collection<String> pilotNames) {
        Set<String> result = new HashSet<>();
        for (String pilotName : pilotNames) {
            String validName = pilotName.replaceAll("[\\n\\r\\t]", "").trim();
            if (validName.length() > 3 && validName.length() < 40) {
                result.add(validName);
            }
            else {
                m_logger.debug("Filtering out pilot with name '" + pilotName + "'");
            }
        }
        return result;
    }
}
