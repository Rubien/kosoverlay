/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.view.fx.controllers;

import com.beimin.eveapi.account.apikeyinfo.ApiKeyInfoParser;
import com.beimin.eveapi.account.apikeyinfo.ApiKeyInfoResponse;
import com.beimin.eveapi.core.ApiAuthorization;
import com.beimin.eveapi.exception.ApiException;
import com.quickcamel.game.eve.kosoverlay.configuration.ConfigKeyConstants;
import com.quickcamel.game.eve.kosoverlay.configuration.IConfigChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

/**
 * Controller functionality for the settings tab - orientated around API and standings considerations
 *
 * @author Louis Burton
 */
public class SettingsTabStandingsController extends SettingsTabController {

    private static final Logger m_logger = LoggerFactory.getLogger(SettingsTabStandingsController.class);

    public static final String NAME_TOOLTIP = "Used to drive the current user's context (Corp, Alliance, etc.)";
    public static final String API_TOOLTIP = "Only requires Contact List privileges (Access Mask 16). Needed for standings consideration.";
    public static final String ONLY_CHECK_UNKNOWN_TOOLTIP = "If you only want to KOS Check pilots for which you have no standings towards.\n" +
            "This reduces the work for the CVA KOS API, and increases the workload on the EVE API by filtering pilots up front.\n" +
            "Requires API details.";
    public static final String NO_NEGATIVE_STANDINGS_IN_LARGE_BATCH_TOOLTIP = "If you only want to see and hear about KOS results from large batches which don't already show as red from standings.\n" +
            "Implicit if only checking unknown pilots. Requires API details.";
    public static final String STANDINGS_USED_TOOLTIP = "If you only want to consider standings results from your alliance and/or corporation and/or yourself.\n" +
            "Check the boxes as desired.";

    @FXML
    private TextField m_characterName;

    @FXML
    private TextField m_apiKeyId;

    @FXML
    private TextField m_apiVerificationCode;

    @FXML
    private CheckBox m_onlyCheckUnknown;

    @FXML
    private CheckBox m_noNegativeStandingsInLargeBatches;

    @FXML
    private CheckBox m_useAlliance;

    @FXML
    private CheckBox m_useCorp;

    @FXML
    private CheckBox m_usePersonal;

    @Resource(name = "currentUserContext")
    private IConfigChangeListener m_apiConfigListener;

    @FXML
    public void initialize() {
        m_logger.debug("Initialising");
        m_characterName.setText(m_configManager.getValue(ConfigKeyConstants.API_CHARACTERNAME));
        m_apiKeyId.setText(m_configManager.getValue(ConfigKeyConstants.API_KEYID));
        m_apiVerificationCode.setText(m_configManager.getValue(ConfigKeyConstants.API_VCODE));
        m_onlyCheckUnknown.setSelected(Boolean.valueOf(m_configManager.getValue(ConfigKeyConstants.BATCHSERVICE_ONLYCHECKNOSTANDINGS, ConfigKeyConstants.BATCHSERVICE_ONLYCHECKNOSTANDINGS_DEFAULT)));
        m_noNegativeStandingsInLargeBatches.setSelected(Boolean.valueOf(m_configManager.getValue(ConfigKeyConstants.BATCHSERVICE_LARGEBATCH_NONEGATIVESTANDINGS, ConfigKeyConstants.BATCHSERVICE_LARGEBATCH_NONEGATIVESTANDINGS_DEFAULT)));
        m_useAlliance.setSelected(Boolean.valueOf(m_configManager.getValue(ConfigKeyConstants.STANDINGS_USEALLIANCE, ConfigKeyConstants.STANDINGS_USEALLIANCE_DEFAULT)));
        m_useCorp.setSelected(Boolean.valueOf(m_configManager.getValue(ConfigKeyConstants.STANDINGS_USECORP, ConfigKeyConstants.STANDINGS_USECORP_DEFAULT)));
        m_usePersonal.setSelected(Boolean.valueOf(m_configManager.getValue(ConfigKeyConstants.STANDINGS_USEPERSONAL, ConfigKeyConstants.STANDINGS_USEPERSONAL_DEFAULT)));
        updateNoNegativeStandingsInLargeBatches();
    }

    public void updateNoNegativeStandingsInLargeBatches() {
        m_noNegativeStandingsInLargeBatches.setDisable(m_onlyCheckUnknown.isSelected());
    }

    public String validate() {
        String errorText = null;
        try {
        if (m_apiKeyId.getText() != null && m_apiKeyId.getText().trim().length() > 0
                || m_apiVerificationCode.getText() != null && m_apiVerificationCode.getText().trim().length() > 0) {
            ApiAuthorization auth = new ApiAuthorization(Integer.parseInt(m_apiKeyId.getText().trim()), m_apiVerificationCode.getText().trim());
            ApiKeyInfoResponse response = ApiKeyInfoParser.getInstance().getResponse(auth);
            if (response.hasError()) {
                if (response.getError() != null && response.getError() != null) {
                    errorText = "Couldn't validate API Key : " + response.getError().getError();
                }
                else {
                    errorText = "Unknown error occurred whilst checking the API Key.";
                }
            }
            else {
                long accessMask = ApiKeyInfoParser.getInstance().getResponse(auth).getAccessMask();
                if ((accessMask & 16) == 0) {
                    errorText = "Please ensure the Contact List is retrievable from this API Key and try again.";
                }
            }
        }
        }
        catch (ApiException e) {
            errorText = "A fatal API error has occurred.";
        }
        return errorText;
    }

    public void save() {
        m_logger.debug("Saving settings");
        boolean needsCurrentUserContextUpdate = updateValueIfChanged(ConfigKeyConstants.API_CHARACTERNAME, m_characterName.getText());
        needsCurrentUserContextUpdate = (updateValueIfChanged(ConfigKeyConstants.API_KEYID, m_apiKeyId.getText()) || needsCurrentUserContextUpdate);
        needsCurrentUserContextUpdate = (updateValueIfChanged(ConfigKeyConstants.API_VCODE, m_apiVerificationCode.getText()) || needsCurrentUserContextUpdate);
        needsCurrentUserContextUpdate = (updateValueIfChanged(ConfigKeyConstants.STANDINGS_USEALLIANCE, String.valueOf(m_useAlliance.isSelected())) || needsCurrentUserContextUpdate);
        needsCurrentUserContextUpdate = (updateValueIfChanged(ConfigKeyConstants.STANDINGS_USECORP, String.valueOf(m_useCorp.isSelected())) || needsCurrentUserContextUpdate);
        needsCurrentUserContextUpdate = (updateValueIfChanged(ConfigKeyConstants.STANDINGS_USEPERSONAL, String.valueOf(m_usePersonal.isSelected())) || needsCurrentUserContextUpdate);
        updateValueIfSet(ConfigKeyConstants.BATCHSERVICE_ONLYCHECKNOSTANDINGS, String.valueOf(m_onlyCheckUnknown.isSelected()));
        updateValueIfSet(ConfigKeyConstants.BATCHSERVICE_LARGEBATCH_NONEGATIVESTANDINGS, String.valueOf(m_noNegativeStandingsInLargeBatches.isSelected()));

        initialize();
        // TODO - clever auto registration of listeners to properties would be nice
        if (needsCurrentUserContextUpdate) {
            m_apiConfigListener.notifyConfigChange();
        }
    }
}
