/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service.user;

import com.beimin.eveapi.character.contact.list.ContactListParser;
import com.beimin.eveapi.character.contact.list.ContactListResponse;
import com.beimin.eveapi.core.ApiAuthorization;
import com.beimin.eveapi.exception.ApiException;
import com.beimin.eveapi.shared.contacts.EveContact;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Updates character's standings from their player, corporation and alliance contact lists, in accordance with cache refresh time.
 *
 * @author Louis Burton
 */
public class StandingsTask extends TimerTask {

    private static final Logger m_logger = LoggerFactory.getLogger(StandingsTask.class);

    private com.beimin.eveapi.character.contact.list.ContactListParser m_standingsParser;
    private ApiAuthorization m_apiAuthorization;
    private Timer m_standingsTimer;
    private CurrentUserContext m_context;
    private boolean m_useAlliance;
    private boolean m_useCorp;
    private boolean m_usePersonal;

    protected StandingsTask(ApiAuthorization auth, Timer timer, CurrentUserContext contextParent, boolean useAlliance,
                            boolean useCorp, boolean usePersonal) {
        m_apiAuthorization = auth;
        m_standingsParser = ContactListParser.getInstance();
        m_standingsTimer = timer;
        m_context = contextParent;
        m_useAlliance = useAlliance;
        m_useCorp = useCorp;
        m_usePersonal = usePersonal;
    }

    @Override
    public void run() {
        try {
            m_logger.debug("About to refresh Standings");
            ContactListResponse response = m_standingsParser.getResponse(m_apiAuthorization);
            if (response.hasError()) {
                m_logger.error("Error refreshing standings : " + response.getError());
            }
            else {
                Map<Long, Double> standings = new HashMap<>();
                if (m_usePersonal) {
                    if (response.getContactList() != null) {
                        for (EveContact contact : response.getContactList()) {
                            standings.put((long) contact.getContactID(), contact.getStanding());
                        }
                    }
                }
                if (m_useCorp) {
                    if (response.get("corporateContactList") != null) {
                        for (EveContact contact : response.get("corporateContactList")) {
                            standings.put((long) contact.getContactID(), contact.getStanding());
                        }
                    }
                }
                if (m_useAlliance) {
                    if (response.get("allianceContactList") != null) {
                        for (EveContact contact : response.get("allianceContactList")) {
                            standings.put((long) contact.getContactID(), contact.getStanding());
                        }
                    }
                }
                m_context.setStandings(standings);

                if (m_logger.isDebugEnabled()) {
                    m_logger.debug("Scheduling next refresh for : " + response.getCachedUntil());
                }
                m_standingsTimer.schedule(new StandingsTask(m_apiAuthorization, m_standingsTimer, m_context, m_useAlliance, m_useCorp, m_usePersonal), response.getCachedUntil());
            }
        }
        catch (ApiException e) {
            m_logger.error("Unable to refresh character contact list", e);
        }
    }

}
