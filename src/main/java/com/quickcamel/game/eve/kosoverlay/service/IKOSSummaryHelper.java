/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service;

import com.quickcamel.game.eve.kosoverlay.service.dto.*;

/**
 * Helps builds up a KOSStatusSummary based on provided information
 *
 * @author Louis Burton
 */
public interface IKOSSummaryHelper {

    void updateSummaryFromPilot(KOSPilotResultDTO pilot, KOSStatusSummary kosStatusSummary);

    void updateSummaryFromCorp(KOSCorpResultDTO corp, KOSStatusSummary kosStatusSummary, boolean lastCorp);

    void updateSummaryFromAlliance(KOSAllianceResultDTO alliance, KOSStatusSummary kosStatusSummary, boolean lastAlliance);

}
