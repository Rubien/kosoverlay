/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Description: Overview of broken down results
 *
 * @author Louis Burton
 */
public class KOSStatusSummary {

    private EntityStatus m_pilotStatus = EntityStatus.UNKNOWN;
    private EntityStatus m_corpStatus = EntityStatus.UNKNOWN;
    private EntityStatus m_allianceStatus = EntityStatus.UNKNOWN;
    private EntityStatus m_overrideStatus = null;
    private String m_shortReason;
    private boolean m_isWarning = false;

    public EntityStatus getPilotStatus() {
        return m_pilotStatus;
    }

    public void setPilotStatus(EntityStatus pilotStatus) {
        m_pilotStatus = pilotStatus;
    }

    public EntityStatus getCorpStatus() {
        return m_corpStatus;
    }

    public void setCorpStatus(EntityStatus corpStatus) {
        m_corpStatus = corpStatus;
    }

    public EntityStatus getAllianceStatus() {
        return m_allianceStatus;
    }

    public void setAllianceStatus(EntityStatus allianceStatus) {
        m_allianceStatus = allianceStatus;
    }

    public String getShortReason() {
        return m_shortReason;
    }

    public void setShortReason(String shortReason) {
        m_shortReason = shortReason;
    }

    public void setOverrideStatus(EntityStatus overrideStatus) {
        m_overrideStatus = overrideStatus;
    }

    public boolean isWarning() {
        return m_isWarning;
    }

    public void setWarning(boolean warning) {
        m_isWarning = warning;
    }

    public EntityStatus getResolvedStatus() {
        EntityStatus result;
        if (m_overrideStatus != null) {
            result = m_overrideStatus;
        }
        else {
            result = EntityStatus.NEUTRAL;
            if (m_allianceStatus.overrules(result)) {
                result = m_allianceStatus;
            }
            if (m_corpStatus.overrules(result)) {
                result = m_corpStatus;
            }
            if (m_pilotStatus.overrules(result)) {
                result = m_pilotStatus;
            }
        }
        return result;
    }

    public String toString() {
        return new ToStringBuilder(this).
                append("pilotStatus", m_pilotStatus).
                append("corpStatus", m_corpStatus).
                append("allianceStatus", m_allianceStatus).
                append("overrideStatus", m_overrideStatus).
                append("shortReason", m_shortReason).
                append("isWarning", m_isWarning).
                toString();
    }
}
